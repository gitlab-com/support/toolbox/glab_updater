# BEFORE YOU BEGIN

- Install and initialize [Google Cloud CLI](https://cloud.google.com/sdk/docs/install-sdk) (commands will work on a project selected in `gcloud` CLI)
- Open the script in an editor of your choice and change `myname` from `lkorbasiewicz` to whatever you use. 
- Add `glab` binary location to your `PATH` and don't forget to `chmod +x` it. 

## USAGE

### glab on

Turns on all instances that names contain `myname` variable value

### glab off

Turns them off

### glab list

Lists all instances containing `myname`

### glab all

List all instances in a project

### sudo glab update

Updates `/etc/hosts` file to map external IP addresses of all instances containing 'myname'
